Field Chart 7.x-1.0alpha2, xxxx-xx-xx
--------------------------
- Externalize all form related functions in field_chart.form.inc
- When field is multiple, dont save empty items
- When table has empty row, don't save it
- Fix an issue when adding row or line to a saved item