Installation
------------

Copy the module folder to your module directory, enable it on the admin module
page.
Download Highcharts from http://www.highcharts.com/download and place the folder
within your librairies folder (usually sites/all/libraries/highcharts)
Check settings on module configuration page (admin/config/content/chart_field)
and define the path to highcharts.js file :
  - sites/all/libraries/highcharts/js/highcharts.js
Create a new field "chart" for your entity.

Author
------
Kevin Gautreau
Twitter : @kgaut
Email : contact@kgaut.net
