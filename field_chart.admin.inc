<?php
function field_chart_settings_form($form, &$form_state) {
  $form['field_chart_highchart_path'] = array(
    '#type' => 'textfield',
    '#title' => t('path to highchart.js file'),
    '#default_value' => variable_get('field_chart_highchart_path', ltrim(libraries_get_path('highcharts', TRUE),'/')),
    '#description' => t('ie :  #path',array('#path'=>'sites/all/libraries/highcharts/js/highcharts.js')),
  );
  return system_settings_form($form);
}

function field_chart_settings_form_validate($form, &$form_state) {
  $file = $form_state['values']['field_chart_highchart_path'];
  if(!is_file($file)) {
    form_set_error('field_chart_highchart_path',t('This file doesn\'t exist'));
  }
  if(pathinfo($file, PATHINFO_EXTENSION) != 'js') {
    form_set_error('field_chart_highchart_path',t('You have to point to the js file'));
  }
}