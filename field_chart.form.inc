<?php

function field_chart_field_widget_form_default(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element = field_chart_field_widget_form_base($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);

  return $element;
}

function field_chart_field_widget_form_base(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element['chart']['type'] = array(
    '#type' => 'select',
    '#options' => field_chart_field_widget_form_get_types(),
    '#title' => t('Type'),
    '#default_value' => isset($items[$delta]['type']) ? $items[$delta]['type'] : NULL,
    '#empty_option' => t('Choose one')
  );

  $element['chart']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Chart\'s title'),
    '#maxlength' => 255,
    '#default_value' => isset($items[$delta]['title']) ? $items[$delta]['title'] : NULL,
  );

  $element['chart']['subtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Chart Subtitle'),
    '#maxlength' => 255,
    '#default_value' => isset($items[$delta]['subtitle']) ? $items[$delta]['subtitle'] : NULL,
  );

  $element['chart']['x_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Absciss Title'),
    '#maxlength' => 255,
    '#default_value' => isset($items[$delta]['x_title']) ? $items[$delta]['x_title'] : NULL,
  );

  $element['chart']['y_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Ordinate Title'),
    '#maxlength' => 255,
    '#default_value' => isset($items[$delta]['y_title']) ? $items[$delta]['y_title'] : NULL,
  );

  $element['chart']['datas'] = array(
    '#title' => t('data header'),
    '#type' => '#markup',
    '#prefix' => '<div id="chart-datas"><label>'.t('Graph data').'</label><table>',
    '#suffix' => '</table></div>',
    '#tree' => TRUE,
  );

  if(!isset($form_state['data'][$delta]['series'])) {
    $form_state['data'][$delta]['series'] = isset($items[$delta]['series']) ? json_decode($items[$delta]['series']) : array("","");
  }

  if(!isset($form_state['data'][$delta]['categories'])) {
    $form_state['data'][$delta]['categories'] = isset($items[$delta]['x_categories']) ? json_decode($items[$delta]['x_categories']) : array("","");
  }

  if(isset($form_state['triggering_element']['#name'])) {
    $button_name = $form_state['triggering_element']['#name'];
    $button_delta = $form_state['triggering_element']['#attributes']['rel'];
    if($button_name == 'add_series') {
      $form_state['data'][$button_delta]['series'][] = "";
    }
    if($button_name == 'add_categories') {
      $form_state['data'][$button_delta]['categories'][] = "";
    }
  }

  $series = $form_state['data'][$delta]['series'];
  $categories = $form_state['data'][$delta]['categories'];

  $nb_series = count($series);
  $nb_categories = count($categories);
  for($i = 0; $i < $nb_categories; $i++) {
    $element['chart']['datas']['x_categories'][$i] = array(
      '#type' => 'textfield',
      '#size' => '10',
      '#prefix' => $i == 0 ? '<tr><th>'.t('↓ Serie\'s title | Date →').'</th><th>' : '<th>',
      '#suffix' => '</th>',
      '#default_value' => array_shift($categories),
    );
  }

  $element['chart']['datas']['x_categories'][$i]['add_category'] = array(
    '#type' => 'button',
    '#value' => t('+'),
    '#prefix' => '<th>',
    '#suffix' => '</th></tr>',
    '#name' => 'add_categories',
    '#limit_validation_errors' => array(),
    '#attributes' => array(
      'rel' => $delta,
    ),
    '#ajax' => array(
      'callback' => 'field_chart_field_widget_form_add_data',
      'wrapper' => 'chart-datas',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  for($i = 0; $i < $nb_series ;$i++) {
    $serie = array_shift($series);
    $element['chart']['datas']['series'][$i]['name'] = array(
      '#type' => 'textfield',
      '#default_value' => isset($serie->name) ? $serie->name : NULL,
      '#size' => '10',
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    );
    $serie->data = (Array) $serie->data;
    for($j = 0; $j < $nb_categories; $j++) {
      $element['chart']['datas']['series'][$i]['data'][$j] = array(
        '#type' => 'textfield',
        '#default_value' => isset($serie->data[$j]) ? $serie->data[$j] : NULL,
        '#size' => '10',
        '#prefix' =>'<td>',
        '#suffix' => $nb_categories == $j ? '</td><td>&nbsp;</td></tr>' : '</td>',
      );
    }
  }

  $element['chart']['datas']['series'][$nb_series+1][$delta]['add_series'] = array(
    '#type' => 'button',
    '#value' => t('+'),
    '#name' => 'add_series',
    '#prefix' => '<tr><td colspan="'.($nb_categories+2).'">',
    '#suffix' => '</td></tr>',
    '#attributes' => array(
      'rel' => $delta,
    ),
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'callback' => 'field_chart_field_widget_form_add_data',
      'wrapper' => 'chart-datas',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  // Collapse existing entries, for some UX.
  if (($delta < sizeof($items))) {
    $fieldset_title = t('Chart "@chart_title"',array('@chart_title' => $items[$delta]['title']));
  }
  else {
    $fieldset_title = t("New Chart");
  }
  $element += array('#type' => 'fieldset');
  $element['#title'] = t('@fieldset_title', array('@fieldset_title' => $fieldset_title));
  $element += array(
    '#collapsible' => false,
    '#collapsed' => false,
  );
  return $element;
}

function field_chart_field_widget_form_add_data($form,&$form_state) {
  $form_state['rebuild'] = true;
  $parents = $form_state['triggering_element']['#array_parents'];
  return $form[$parents[0]][$parents[1]][$parents[2]][$parents[3]][$parents[4]];
}

function field_chart_field_widget_form_get_types() {
  return array(
    'line' => t('Line chart'),
    'area' => t('Area chart'),
    'bar' => t('basic bar chart'),
    'stacked_bars' => t('Stacked bars chart'),
    'column' => t('basic columns chart'),
    'stacked_columns' => t('Stacked columns chart'),
    'synchronised' => t('synchronised chart'),
    'tree_map' => t('tree map with color axis'),
  );
}