<?php

function field_chart_field_formatter_view_default($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  drupal_add_js(variable_get('field_chart_highchart_path'));
  foreach ($items as $delta => $item) {
    $json_object = new stdClass();

    $json_object->chart = new stdClass();
    $json_object->chart->type = $item['type'];

    $json_object->title = new stdClass();
    $json_object->title->text = $item['title'];

    $json_object->subtitle = new stdClass();
    $json_object->subtitle->text = $item['subtitle'];

    $json_object->xAxis = new stdClass();
    $json_object->xAxis->title = new stdClass();
    $json_object->xAxis->title->text = $item['x_title'];
    $json_object->xAxis->categories = json_decode($item['x_categories']);

    $json_object->yAxis = new stdClass();
    $json_object->yAxis->title = new stdClass();
    $json_object->yAxis->title->text = $item['y_title'];

    $json_object->series = json_decode($item['series']);
    $item_output = '<div id="highcharts-container-' . $delta . '" style="width: 100%; height: 400px; margin: 0 auto"></div>';

    drupal_add_js('jQuery(document).ready(function(){jQuery("#highcharts-container-' . $delta . '").highcharts(' . json_encode($json_object) . ')});', 'inline');

    $element[$delta]['chart_row'] = array(
      '#type' => 'fieldset',
    );
    $element[$delta]['chart_row']['content'] = array(
      '#markup' => $item_output,
    );
  }
  return $element;
}